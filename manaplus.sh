#!/usr/bin/env bash

dll_check(){
    # check all but windows own dll's
    windlls=("wldap32.dll opengl32.dll gdi32.dll advapi32.dll kernel32.dll msvcrt.dll wsock32.dll shell32.dll crypt32.dll ws2_32.dll user32.dll iphlpapi.dll winmm.dll")
    striped=${dlls#"Missing dll files: "};
    dlls=`tr -d ',' <<< $striped`
    err=""
    for dll in $dlls
    do
        [[ ! ${windlls[*]} =~ $dll ]] && err="$err $dll";
    done
    [[ "$err" != "" ]] && echo "ERR: missing: $err" && exit 1 || return;
}

# CI_COMMIT_TAG="v2.1.1.1"  # <- test version
# CI_COMMIT_TAG="s20210101" # <- test snapshot2
get_version_by_tag(){
    latest_tag=`git -c 'versionsort.suffix=-' \
        ls-remote --exit-code --refs --sort='version:refname' --tags ${MP_REPOSITORY} '*.*.*' \
        | tail --lines=1 \
        | cut --delimiter='/' --fields=3`
    IFS='.' read -r -a array <<< "$CI_COMMIT_TAG" # splits commit tag in array
    [ "${CI_COMMIT_TAG:0:1}" == "v" ] \
        && V="${array[0]:1}.${array[1]}.${array[2]}.${array[3]}" # add dots to version
    [ "${CI_COMMIT_TAG:0:1}" == "s" ] \
        && V="snapshot-${CI_COMMIT_TAG:1}" # just replace 's' with 'snapshot-'
    [ "${CI_COMMIT_TAG}"     == ""  ] \
        && echo "!WARN: no tag set, using test-0.0.0.1!" \
        && echo "latest tag: '${latest_tag}' on '${MP_REPOSITORY}' master" \
        && V="test-0.0.0.1" # test versions, (not running from tags)
    export PRODUCT_VERSION=$V
    export VERSION=$V
    echo "PRODUCT_VERSION, VERSION = '$V'"
}

make_portable(){
    dll_dir=$1 # handled different, needs to get cp before all others! (to /tmp/Mana/%)
    root_dir="/manaplus/"
    files_list=("build/tmp/src/manaplusd.exe build/tmp/src/manaplus.exe packaging/windows/portable.xml data/ docs/ AUTHORS COPYING NEWS README.txt") # build/tmp/src/gdb.exe (already included in dll_dir)
    tmp_dir="/tmp/Mana/"
    debug_files="${tmp_dir}manaplusd.exe ${tmp_dir}gdb.exe"
    mkdir -p $tmp_dir
    cp ${dll_dir}* $tmp_dir

    files=`tr -d ',' <<< $files_list`
    for file in $files
    do
        cp -r $root_dir$file $tmp_dir
    done

    cd ${root_dir}packaging/windows/ && ./make-translations.sh
    cp -r $root_dir/translations/ ${tmp_dir}
    cd /tmp
    zip -r debug-manaplus-$VERSION.zip Mana/
    sha256sum debug-manaplus-$VERSION.zip > debug-sha256checksum.txt
    rm -rf $debug_files
    zip -r manaplus-$VERSION.zip Mana/
    sha256sum manaplus-$VERSION.zip > sha256checksum.txt

}


cp /mxe/usr/i686-w64-mingw32.shared/bin/gdb.exe /libs/dll/
cp /manaplus/tools/ci/scripts/winvars.sh manaplus/build/
cd /manaplus/

patch_file="../manaplus.patch"
if [[ -f $patch_file ]]; then
    git apply $patch_file || exit 1
fi

cd /manaplus/build/
get_version_by_tag
./winmake --enable-commandlinepassword || exit 1
cp /manaplus/build/tmp/src/manaplus.exe /manaplus/build/tmp/src/manaplusd.exe
cp /mxe/usr/i686-w64-mingw32.shared/bin/gdb.exe /manaplus/build/tmp/src/
strip /manaplus/build/tmp/src/manaplus.exe
# profiler (needs another build)

dll_dir="/libs/dll/"

dlls=`/mxe/tools/copydlldeps.py -C/manaplus/build/tmp/src -L/mxe/usr/i686-w64-mingw32.shared/bin/ ${dll_dir}`
dll_check $dlls

make_portable $dll_dir

#./packwin || exit 1 # disabled (not required @mana-launcher)

# artifacts
#cp /manaplus/packaging/windows/manaplus-*.exe ${CI_PROJECT_DIR}/build/ || exit 1
cp /tmp/*.zip ${CI_PROJECT_DIR}/build/ || exit 1 # copy portable client zip files
cp /tmp/*.txt ${CI_PROJECT_DIR}/build/ || exit 1 # copy sha256checksum files
# TODO: checksum [zip->sha->{version?}.sha] ${CI_PROJECT_DIR}/build/
cp /manaplus/build/tmp/config.log ${CI_PROJECT_DIR}/logs/ || true
cp /manaplus/build/make2.log ${CI_PROJECT_DIR}/logs/ || true

