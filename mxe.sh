#!/usr/bin/env bash
export CROSS=i686-w64-mingw32.shared
cross_req="cc smpeg ffmpeg sdl sdl_image sdl_ttf sdl_net sdl_gfx libxml2 curl libpng zlib gettext sdl_mixer gdb"
docker_req="gcc g++ python autoconf automake autopoint bash bison bzip2 flex gettext git gperf intltoolize libtool lzip make makensis openssl p7zip patch perl ruby sed unzip wget lzma"


req_check()
{
    echo "*** sys-info *** >---------------------------------------------------"
    uname -a
    req_err=""
    for req in $docker_req; do
        if ! command -v $req > /dev/null 2>&1; then
          req_err="$req_err $req";  
        fi
    done
    if [ "$req_err" != "" ]; then
        echo "! missing req: $req_err !";
        exit 1;
    fi
    echo "*** sys-info *** <---------------------------------------------------"
}

mxe()
{
    echo "*** mxe-build *** >--------------------------------------------------"
    cd /
    git clone https://github.com/mxe/mxe.git
    cd mxe
    
    # mxe params to settings.mk
    echo "MXE_TARGETS := ${CROSS}" > settings.mk
    echo "LOCAL_PKG_LIST := ${cross_req}" >> settings.mk
    echo ".DEFAULT_GOAL  := local-pkg-list" >> settings.mk
    echo 'local-pkg-list: $(LOCAL_PKG_LIST)' >> settings.mk
    
    # mxe patch
    git apply ../mxe.patch || exit 1

    # make mxe
    make || exit 1

    # sdl patch
    sed -i 's|#define GL_GLEXT_VERSION 29|#ifndef GL_GLEXT_VERSION\n#define GL_GLEXT_VERSION 29\n#endif|' /mxe/usr/${CROSS}/include/SDL/SDL_opengl.h
    
    # cleanup downloads & logs
    make clean-junk || exit 1
    cp /mxe/usr/${CROSS}/bin/gdb.exe /build/ || exit 1
    echo "*** mxe-build *** <--------------------------------------------------"
}


test_mxe()
{
    export PATH=/mxe/usr/bin:/mxe/usr/${CROSS}/bin:$PATH
    echo `${CROSS}-gcc --version`
    echo `${CROSS}-g++ --version`
    echo "${PATH}"
}

req_check
mxe
test_mxe
